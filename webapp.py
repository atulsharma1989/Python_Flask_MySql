from flask import Flask, render_template, request
from flask_mysqldb import MySQL
import yaml

webapp = Flask(__name__)

## Loading yaml file for following use.
db_param = yaml.load(open('templates/db_param.yaml'))

## mysql db configuration, following db related param will pass through yaml file
webapp.config['MYSQL_HOST'] = db_param['mysql_host']
webapp.config['MYSQL_USER'] = db_param['mysql_user']
webapp.config['MYSQL_PASSWORD'] = db_param['mysql_password']
webapp.config['MYSQL_DB'] = db_param['mysql_database']

#Instantiation of mysql db to connect with application
mysql = MySQL(webapp)

@webapp.route('/')
def ImportIndex():
         return render_template('index.html')

@webapp.route('/', methods=['GET', 'POST'])

def getvalue():
       if request.method == 'POST':
          content = request.form
          Name = content['Name']
          print Name
          Favorite_Color = content['Favorite_Color']
          Cats_Dogs = content['Cats_Dogs']
          print Cats_Dogs
          cur = mysql.connection.cursor()
          cur.execute(" INSERT INTO web_content(Name, Favorite_Color, Cats_Dogs) VALUES(%s, %s, %s)", (Name, Favorite_Color, Cats_Dogs))
          mysql.connection.commit()
          cur.close()
          return 'Data Has Been Uploaded Successfully To Mysql DataBase'
       return render_template('value.html', n=Name, Favorite_Color=Favorite_Color, Cats_Dogs=Cats_Dogs)
if __name__ == '__main__':
       webapp.run(ssl_context = ('cert.pem', 'key.pem'), host = "10.0.0.2", debug=True)
