#!/bin/python
import mysql.connector
from cryptography.fernet import Fernet

########Importing cryptography module to generate encrypted string to use in mysql#######

key = Fernet.generate_key() #this is your "password"
cipher_suite = Fernet(key)
encoded = cipher_suite.encrypt(b"Mysql@12")
decoded_text = cipher_suite.decrypt(encoded)

print "Printing encrypted string",encoded
print "Printing decrpted value of the encrypted string",decoded_text

########### Closing Encrypted Python Code ################

######## Connecting to database Devops_Test ########

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="Mysql@123",
  auth_plugin='mysql_native_password',
  database="Devops_Test"
)
mycursor = mydb.cursor()
mycursor.execute("CREATE DATABASE Devops_Test")

###### Showing created databases ########
my_db = mydb.cursor()
my_db.execute("SHOW DATABASES")
for x in my_db:
     print x

####### Creating Table in Devops_Test ############
my_table = mydb.cursor()
my_table.execute("CREATE TABLE web_content (Name VARCHAR(30) NOT NULL, Faviorate_Color VARCHAR(15) NOT NULL, Cats_Dogs VARCHAR(15) NOT NULL, UNIQUE (name))")

show_db = mydb.cursor()
show_db.execute("SHOW TABLES")
for table in show_db:
     print  "Showing tables under DataBase Devops_Test",table
